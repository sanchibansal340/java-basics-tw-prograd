import java.util.*;

public class SalaryFinder {
    private static int calculateWeekSalary(int[] hrsWorked) {
        int totalWeekHours = 0, weekSalary = 0;

        for (int i = 1; i < 6; i++) {
            totalWeekHours += hrsWorked[i];
        }

        if (totalWeekHours > 40) {
            weekSalary += (totalWeekHours - 40) * 25;
        }

        return weekSalary;
    }

    private static int calculateSunSalary(int hrsWorkedOnSun) {
        return 50 * hrsWorkedOnSun;
    }

    private static int calculateSatSalary(int hrsWorkedOnSat) {
        return 25 * hrsWorkedOnSat;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] hrsWorked = new int[7];

        int totalSalary = 0;

        for (int i = 0; i < 7; i++) {
            hrsWorked[i] = Integer.parseInt(scanner.nextLine());

            totalSalary += (hrsWorked[i] * 100);

            if (hrsWorked[i] > 8) {
                totalSalary += ((hrsWorked[i] - 8) * 15);
            }
        }

        scanner.close();

        totalSalary += calculateWeekSalary(hrsWorked);

        if (hrsWorked[0] > 0) {
            totalSalary += calculateSunSalary(hrsWorked[0]);
        }
        if (hrsWorked[6] > 0) {
            totalSalary += calculateSatSalary(hrsWorked[6]);
        }

        System.out.println("Total Salary = " + totalSalary);
    }
}
