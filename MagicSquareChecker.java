import java.util.*;

public class MagicSquareChecker {
    private static boolean isMagicSquare(int[][] matrix) {
        int n = matrix.length, d1Sum = 0, d2Sum = 0;

        for (int i = 0; i < n; i++) {
            d1Sum += matrix[i][i];
            d2Sum += matrix[i][n - 1 - i];
        }

        if (d1Sum != d2Sum)
            return false;

        for (int i = 0; i < n; i++) {
            int rowSum = 0, colSum = 0;

            for (int j = 0; j < n; j++) {
                colSum += matrix[j][i];
                rowSum += matrix[i][j];
            }

            if (rowSum != colSum || rowSum != d1Sum) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        int[][] matrix = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = scanner.nextInt();
            }
            scanner.nextLine();
        }

        System.out.println((isMagicSquare(matrix) ? "yes" : "no"));

        scanner.close();
    }
}
