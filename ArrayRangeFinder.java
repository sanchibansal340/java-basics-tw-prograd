import java.util.Scanner;

public class ArrayRangeFinder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        int[] arr = new int[n];

        int maxElem = Integer.MIN_VALUE, minElem = Integer.MAX_VALUE;

        for (int i = 0; i < n; i++) {
            arr[i] = Integer.parseInt(scanner.nextLine());

            maxElem = Math.max(arr[i], maxElem);
            minElem = Math.min(arr[i], minElem);
        }

        int range = maxElem - minElem;

        System.out.println("Range of array = " + range);
        scanner.close();
    }
}
