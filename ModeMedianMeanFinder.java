import java.util.*;

public class ModeMedianMeanFinder {
    private static double getMean(int[] arr) {
        int sum = 0, n = arr.length;

        for (int element : arr) {
            sum += element;
        }

        return (double) sum / (double) n;
    }

    private static double getMedian(int[] arr) {
        int n = arr.length;
        Arrays.sort(arr);

        if (n % 2 != 0) {
            return arr[n / 2];
        }

        return (double) (arr[(n - 1) / 2] + arr[n / 2]) / 2.00;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = Integer.parseInt(scanner.nextLine());
        }

        double mean = getMean(arr);
        double median = getMedian(arr);
        double mode = ((3 * median) - (2 * mean));

        System.out.println("Mean of array = " + String.format("%.2f", mean));
        System.out.println("Median of array = " + String.format("%.2f", median));
        System.out.println("Mode of array = " + String.format("%.2f", mode));

        scanner.close();
    }
}
