import java.util.*;

public class MangoTreeChecker {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int row = Integer.parseInt(scanner.nextLine());
        int col = Integer.parseInt(scanner.nextLine());
        int treeNumber = Integer.parseInt(scanner.nextLine());

        if (treeNumber <= (row * col)) {
            // Assuming treeNumber starting from 1
            if ((treeNumber >= 1 && treeNumber <= col) || treeNumber % col == 0 || treeNumber % col == 1) {
                System.out.println("yes");
            } else {
                System.out.println("no");
            }
        } else {
            System.out.println("no");
        }

        scanner.close();
    }
}